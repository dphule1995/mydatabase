package com.example.admin.mydatabase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Admin on 13/02/2018.
 */

public class MyDbHelper extends SQLiteOpenHelper {
    private static final String tableName="login";
     public MyDbHelper(Context context) {
        super(context, "Login.db", null, 2);//2nd argument is dbName and
                                                                // 3rd parameter is factory which is always null and
                                                                 // 4th argument is version of database.
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String table="create table "+ tableName+"(username text,password text)";
        db.execSQL(table);
  }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
         db.execSQL("drop table if exists "+tableName);
         onCreate(db);
    }


}
