package com.example.admin.mydatabase;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    EditText userName,passWord;
    String user,pass;
    MyDbHelper dbHelper;
    ListView lv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dbHelper = new MyDbHelper(this);
        findId();

    }

    public void findId()
    {
        userName=(EditText)findViewById(R.id.userName);
        passWord=(EditText)findViewById(R.id.passWord);
        lv=(ListView)findViewById(R.id.lv);
    }
    public void getValues()
    {
        user=userName.getText().toString();
        pass=passWord.getText().toString();
    }
    public void login(View v)
    {
        try {
            getValues();

            SQLiteDatabase db = dbHelper.getWritableDatabase();

            ContentValues cv = new ContentValues();
            cv.put("username", user);
            cv.put("password", pass);

            long row = db.insert("login", null, cv);

            Toast.makeText(this, "inserted Succefully", Toast.LENGTH_SHORT).show();
            System.out.println("row number =" + row);


        }
        catch (Exception e)
        {
            Toast.makeText(this, "msg="+e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void fetchValues(View v)
    {
        Cursor c= null;
        try
        {
            SQLiteDatabase db = dbHelper.getReadableDatabase();
            ArrayList<String> arr=new ArrayList<String>();

             c=db.rawQuery("select * from login",null);
             c.moveToFirst();
            do
            {
                arr.add("username="+c.getString(0)+"\n"+"password="+c.getString(1));
                lv.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_expandable_list_item_1,arr));

            }while(c.moveToNext());

        }
        catch(Exception e)
        {
            Toast.makeText(this, "msg="+e.getMessage(), Toast.LENGTH_SHORT).show();
        }

        finally
        {
            c.close();
        }


    }
}
